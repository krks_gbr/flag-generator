

import controlP5.CallbackEvent;
import controlP5.CallbackListener;
import controlP5.ControlP5;
import processing.core.PApplet;
import processing.core.PGraphics;

public class UI {


    public interface ValueListener {
        void listen(float value);
    }



    ControlP5 cp5;

    int controlY = 5;
    int sliderHeight = 30;
    int toggleHeight = 10;
    int gutter = 20;
    int x, y, width, height;
    int colorWheelY = 10;
    int colorWheelX = 400;
    int buttonX = 150, buttonY = 250;

    PGraphics g;
    public UI(PApplet applet, int x, int y, int width, int height){
        cp5 = new ControlP5(applet);
        this.x = x+10;
        this.y = y;
        this.width = width;
        this.height = height;
        this.g = applet.g;
        colorWheelX+=x;
        colorWheelY+=y;
        buttonX += x;
        buttonY += y;
    }

    UI colorWheel(String label, ValueListener listener){
        cp5.addColorWheel(label)
                .setPosition(colorWheelX, colorWheelY)
                .onChange(callbackEvent -> listener.listen(callbackEvent.getController().getValue()));

        colorWheelY += 250 + gutter;
        return this;
    }

    UI button(String label, ValueListener listener){
        cp5.addButton(label)
                .setValue(100)
                .setPosition(buttonX, buttonY)
                .setSize(200,50)
                .onRelease(callbackEvent -> listener.listen((float)1.0))
        ;
        buttonY += 50 + gutter;
        return this;
    }

    UI slider(String label, float min, float max,  ValueListener listener) {
        cp5.addSlider(label)
                .setWidth(200)
                .setHeight(sliderHeight)
                .setPosition(x + 10, controlY)
                .setRange(min, max)
                .onChange(callbackEvent -> listener.listen(callbackEvent.getController().getValue()))
        ;
        controlY += sliderHeight + gutter;
        return this;
    }

    UI toggle(String label, boolean value, ValueListener listener){
        // create a toggle and change the default look to a (on/off) switch look
        cp5.addToggle(label)
                .setPosition(x+10, controlY)
                .setSize( 50, toggleHeight )
                .setValue(value)
                .setMode(ControlP5.SWITCH)
                .onChange(event -> listener.listen(event.getController().getValue()))
        ;
        controlY += toggleHeight + gutter;
        return this;
    }


    public void draw(){
        g.pushStyle();
        g.noStroke();
        g.fill(0);
        g.rect(x, y, width, height);
        g.popStyle();
    }

}
