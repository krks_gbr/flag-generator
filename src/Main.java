

import processing.core.PApplet;
import java.io.File;
import java.util.*;

import processing.core.PGraphics;
import processing.pdf.*;


public class Main extends PApplet {



    public static void main(String[] args) {
        PApplet.main("Main");
    }


    public void settings() {
        size(1440, 800);
    }

    UI ui;
    Drawer drawer;

    public void setup(){
        shapeMode(CENTER);



        Map<String, Icon> icons = new HashMap<>();
        listIcons().forEach((label, path) -> icons.put(label, new Icon(loadShape(path), label )));
        drawer = new Drawer(this, icons, 0, 0, width/2, height);

        ui = new UI(this, width/2, 0, width/2, height)
                .slider("scaleStep", 2, width, value -> drawer.setScaleStep(value))
                .slider("scaleStepExponent", (float)0.01, 10, value -> drawer.setScaleStepExponent(value))
                .slider("height", 0, height*4, value -> drawer.setIconHeight(value))
                .toggle("disableStyle", false, value -> drawer.toggleStyle())
                .colorWheel("primaryColor", value  -> drawer.setPrimaryColor((int)value))
                .colorWheel("secondaryColor", value -> drawer.setSecondaryColor((int)value))
                .colorWheel("backgroundColor", value-> drawer.setBackgroundColor((int)value))
                .button("shuffle", value -> drawer.shuffleStack())
                .button("save pdf", value -> savePDF())
        ;


        icons.keySet().forEach(label -> ui.toggle(label, false, value ->  drawer.toggleIcon(label) ));


    }

    private void savePDF() {
        String file = "/Users/GaborK/projects/GRADUATION_KABK_2016/code/flaggenerator/pdf/" + System.currentTimeMillis() + ".pdf";
        PGraphics pdf = createGraphics(drawer.width, drawer.height, PDF, file);
        pdf.shapeMode(CENTER);
        pdf.beginDraw();
        drawer.draw(pdf);
        pdf.dispose();
        pdf.endDraw();
        System.out.println("saved pdf");
    }


    public void draw(){
        drawer.draw(g);
        ui.draw();
    }

    public void keyReleased(){

        int n = Character.getNumericValue(key);
        if(n >= 0 && n<=9){
            drawer.selectFlag(n);
        }

    }





    Map<String, String> listIcons(){
        File[] files = new File("data").listFiles((dir, name) -> {
            return name.endsWith(".svg");
        });

        Map<String, String> iconList = new HashMap<>();
        Arrays.asList(files).forEach(file -> iconList.put( file.getName(), file.getAbsolutePath() ));
        return iconList;
    }

}
