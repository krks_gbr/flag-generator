import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PGraphics;

import java.util.*;




public class Drawer {

    PApplet parent;
    private Map<String, Icon> icons = new HashMap<>();
    private List<Icon> visibleIcons = new ArrayList<>();
    int x, y, width, height;
    private boolean style = true;
    private int primaryColor, secondaryColor;
    private int backgroundColor = 255;
    private float iconHeight = 0;
    private float scaleStep;
    private float stepScaleExponent = 1;
    private int selectedFlag = -1;
    FlagDrawer flagDrawer;

    public Drawer(PApplet parent, Map<String, Icon> icons, int x, int y, int width, int height){

        this.parent = parent;
        this.icons = icons;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        primaryColor = parent.color(255);
        secondaryColor = parent.color(0);
        scaleStep = width/6;
        flagDrawer = new FlagDrawer();

        System.out.printf("instantiated drawer with " + icons.size() + " icons");
    }


    void toggleIcon(String label){
        Icon icon = icons.get(label);
        icons.get(label).toggleVisibility();
        visibleIcons.removeIf(ic -> !ic.visible);
        if(icon.visible){
            visibleIcons.add(icon);
        }
    }

    void setIconHeight(float height){
        this.iconHeight = height;
    }

    void setScaleStep(float scaleStep){
        this.scaleStep = scaleStep;
    }

    void setScaleStepExponent(float exp){
        stepScaleExponent = exp;
    }

    void shuffleStack(){
        Collections.shuffle(visibleIcons);
    }

    void draw(PGraphics g){
        g.background(backgroundColor);

        if(selectedFlag != -1){
            flagDrawer.draw(g, selectedFlag);
        }

        for (int i = 0; i < visibleIcons.size(); i++) {
            Icon icon = visibleIcons.get(i);
            int fill = i % 2 == 0 ? primaryColor : secondaryColor;
            int stroke = i%2 == 1 ? primaryColor : secondaryColor;
            g.stroke(stroke);
            g.fill(fill);
            icon.width(100 + (float) Math.pow((visibleIcons.size() - i), stepScaleExponent) * scaleStep)
                    .height(iconHeight)
                    .draw(g, width / 2, height / 2);
        }
    }

    public void toggleStyle() {
        this.style = !this.style;
        System.out.println("style are " + (style ? "enabled" : "disabled"));
        if(style){
            icons.values().forEach(Icon::enableStyle);
        } else {
            icons.values().forEach(Icon::disableStyle);
        }
    }

    public void setPrimaryColor(int primaryColor) {
        this.primaryColor = primaryColor;
    }

    public void setSecondaryColor(int secondaryColor) {
        this.secondaryColor = secondaryColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void selectFlag(int n) {
        selectedFlag = n;
        System.out.println("selected flag: " + n);
    }


    class FlagDrawer {


        public void draw(PGraphics g, int kind){
            g.noStroke();

            g.pushMatrix();

            switch (kind){

                case 0:
                    break;
                case 1:
                    g.fill(255);
                    g.ellipse(x+width/2, y+height/2, width/2, width/2);
                    break;
                case 2:
                    g.fill(primaryColor);
                    g.rect(x, y, width/3, height);

                    g.fill(255);
                    g.rect( x + width/3, y, width/3,  height);

                    g.fill(backgroundColor);
                    g.rect(x+ width/3 * 2, y, width/3, height);
                    break;
                case 3:
                    g.fill(primaryColor);
                    g.rect(x, y, width/2, height);

                    g.fill(backgroundColor);
                    g.rect(x+width/2, y, width/2, height);
                    break;
                case 4:
                    g.fill(primaryColor);
                    g.rect(x, y, width, height/2);

                    g.fill(backgroundColor);
                    g.rect(x, y+height/2, width, height/2);
                    break;
                case 5:
                    int crossWidth = 200;
                    g.fill(255);
                    g.rect(width/2 - crossWidth/2, 0, crossWidth, height);
                    g.rect(x, height/2 - crossWidth/2, width, crossWidth );
                    break;

                case 6:
                    g.fill(primaryColor);
                    g.rect(x, width, y, height/3);

                    g.fill(255);
                    g.rect(x, height/3, height/3, height/3);

                    g.fill(secondaryColor);
                    g.rect(x, width, height/3 * 2, height/3);
                    break;

                case 7:
                    for(int i = 0; i<31; i++){
                        float h = height/30;
                        g.fill(i%2 == 0 ? primaryColor : backgroundColor);
                        g.rect(x, i*h, width, h);
                    }
                    break;

                case 8:
                    g.fill(primaryColor);
                    g.beginShape();
                    g.vertex(x, y);
                    g.vertex(width, y);
                    g.vertex(x, height);
                    g.endShape(PConstants.CLOSE);
                    break;

                case 9:
                    g.fill(primaryColor);
                    g.beginShape();
                    g.vertex(x, y);
                    g.vertex(width/2, y);
                    g.vertex(x, height);
                    g.endShape(PConstants.CLOSE);

                    g.beginShape();
                    g.vertex(width/2, y);
                    g.vertex(width, height);
                    g.vertex(width, y);
                    g.endShape(PConstants.CLOSE);

                    break;

                default:
                    throw new RuntimeException("flag kind " + kind + " not defined.");

            }

            g.popMatrix();

        }



    }




}
