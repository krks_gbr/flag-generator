import processing.core.PGraphics;
import processing.core.PShape;

class Icon {

    PShape shape;
    float ratio;
    float width;
    float height = 0;
    boolean visible = false;
    String label;

    Icon(PShape shape, String label){
        this.label = label;
        this.shape = shape;
        ratio = shape.width/shape.height;
        width = 10;
    }

    Icon toggleVisibility(){
        visible = !visible;
        System.out.println(label + (visible ? " is " : " is not ") + "visible" );
        return this;
    }

    Icon disableStyle(){
        System.out.println("disabled style");
        shape.disableStyle();
        return this;
    }

     Icon enableStyle() {
        System.out.println("enabled style");
        shape.enableStyle();
        return this;
    }


    Icon height(float h){
        height = h;
        return this;
    }

    Icon width(float w){
        width = w;
        return this;
    }


    void draw(PGraphics g, float x, float y){
        float height = this.height == 0 ? width*ratio : this.height;
        g.shape(shape, x, y, width, height);
    }


}
